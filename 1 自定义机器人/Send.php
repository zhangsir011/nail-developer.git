<?php


namespace app\admin\model;
use think\Model;

class Send extends Model
{
    //text
    public function sendMsg(){
        // 机器人hook地址，access_token需要更换成自己的token哦！ 放自己的webhook
        $webhook = "********";
        $message = "登录用户
            老汉sdfdsafdsafdsaf2343214321fdsafdsa";
        $atMobile = ['18792779949'];
        $atUserIds = ['0109541105777953'];
        $isAtAll = false;
        $data = [
            'msgtype' => 'text',
            'text'    =>
                ['content' => $message],
            'at'      => [
                'atMobiles' => $atMobile,
                'atUserIds' => $atUserIds,
                'isAtAll' => $isAtAll,
            ],

        ];
        $data_string = json_encode($data);

        $result = request_by_curl($webhook, $data_string);
        echo $result;
    }

    //markdown
    public function sendMsgMarkdown(){
        // 机器人hook地址，access_token需要更换成自己的token哦！
        $webhook = "https://oapi.dingtalk.com/robot/send?access_token=312afa4ac34971eaffecfce3eb1f1ec9f7b3bdf4a4b390bdd26406ccd0d0a844";
        $message = "
            # HelloWorld
            老汉
        ";
        $message = preg_replace("/^\h+/mu", '', $message);
        $atMobile = ['18792779949'];
        $atUserIds = ['0109541105777953'];
        $isAtAll = false;
        // markdown通知
        $data = [
            'msgtype'  => 'markdown',
            'title'    => '',
            'markdown' => [
                'title' => '注册用户信息',
                'text'  => $message,
            ],
            // 是否艾特全体成员 true 是
            'at'       => [
                'atMobiles' => $atMobile,
                'atUserIds' => $atUserIds,
                'isAtAll' => $isAtAll,
            ],

        ];
        $data_string = json_encode($data);

        $result = request_by_curl($webhook, $data_string);
        echo $result;
    }
}
